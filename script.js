function pad(num, size) {
    var s = parseInt(num) + "";
    while (s.length < size) s = "0" + s;
    return s;
}

var timer = {};

$(function() {
    timer.seconds = 0;

    timer.reset = function(seconds) {
        $('body').css({background: 'black'});
        timer.stop();
        timer.seconds = seconds;
        timer.display();
    }

    timer.display = function() {
        $('#display').text(pad(timer.seconds / 60, 2) + ":" + pad(timer.seconds % 60, 2));
        if(timer.seconds == 0) {
            $('body').css({background: 'red'});
        }
    }

    timer.stop = function() {
        $('#display').css({color: 'gray'});
        clearInterval(timer.interval);
        timer.interval = undefined;
    }

    timer.start = function() {
        $('#display').css({color: 'white'});
        timer.interval = setInterval(function() {
            timer.seconds = Math.max(0, timer.seconds-1);
            timer.display();
        }, 1000);
    }

    timer.toggle = function() {
        if(timer.interval === undefined) {
            timer.start();
        } else {
            timer.stop();
        }
    }
});